from django.views.generic import DetailView, ListView
from .models import Post


class BlogIndexView(ListView):
    model = Post
    template_name = 'blog/blog_index.html'


class BlogDetailView(DetailView):
    model = Post
    template_name = 'blog/post_detail.html'
