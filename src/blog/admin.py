from django.contrib import admin
from .models import Category, Post
from cms.admin.placeholderadmin import PlaceholderAdminMixin

class CategoryAdmin(admin.ModelAdmin):
    pass

class PostAdmin(PlaceholderAdminMixin, admin.ModelAdmin):
    pass

admin.site.register(Category, CategoryAdmin)
admin.site.register(Post, PostAdmin)