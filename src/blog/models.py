from django.db import models
from django.urls import reverse
from .behaviours import SlugableBehaviour, TimestampableBehaviour
from cms.models.fields import PlaceholderField

class Category(TimestampableBehaviour, SlugableBehaviour, models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class Post(TimestampableBehaviour, SlugableBehaviour, models.Model):
    slugable_field = 'title'

    title = models.CharField(max_length=250)
    content = PlaceholderField('post_content', related_name="content")
    introduction = PlaceholderField('post_introduction', related_name="intro")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"
