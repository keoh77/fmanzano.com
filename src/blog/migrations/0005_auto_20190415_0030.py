# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-04-14 22:30
from __future__ import unicode_literals

import cms.models.fields
from django.db import migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0022_auto_20180620_1551'),
        ('blog', '0004_post_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='introduction',
            field=cms.models.fields.PlaceholderField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='intro', slotname='post_introduction', to='cms.Placeholder'),
        ),
        migrations.AlterField(
            model_name='post',
            name='content',
            field=cms.models.fields.PlaceholderField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='content', slotname='post_content', to='cms.Placeholder'),
        ),
    ]
