from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'(?P<slug>[-\w]+)/$', views.BlogDetailView.as_view(), name="detail"),
    url(r'$', views.BlogIndexView.as_view(), name="index"),
]