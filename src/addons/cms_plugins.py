from cms.models import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from .forms import GridColumnPluginForm, GridPluginForm

from .models import HelloPlugin, GridBootstrap, GridColumnBootstrap


@plugin_pool.register_plugin
class HelloPlugin(CMSPluginBase):
    model = HelloPlugin
    name = _('Hello Plugin')
    render_template = "addons/hello_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super(HelloPlugin, self).render(
            context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class GridBootstrapPlugin(CMSPluginBase):
    model = GridBootstrap
    name = _('Grid Row (wrapper)')
    module = _('Multi Columns')
    render_template = 'addons/grid.html'
    allow_children = True
    child_classes = ['GridColumnBootstrapPlugin']
    form = GridPluginForm

    def render(self, context, instance, placeholder):
        context.update({
            'grid': instance,
            'placeholder': placeholder,
        })
        return context

    def save_model(self, request, obj, form, change):
        response = super(
            GridBootstrapPlugin,
            self).save_model(request, obj, form, change)
        if not change:
            for x in range(1, int(form.cleaned_data['create'])+1):
                col = GridColumnBootstrap(
                    parent=obj,
                    placeholder=obj.placeholder,
                    language=obj.language,
                    size_xs=form.cleaned_data.get('create_size_xs', None),
                    size_sm=form.cleaned_data.get('create_size_sm', None),
                    size_md=form.cleaned_data.get('create_size_md', None),
                    size_lg=form.cleaned_data.get('create_size_lg', None),
                    position=CMSPlugin.objects.filter(parent=obj).count(),
                    plugin_type=GridColumnBootstrapPlugin.__name__,
                )
                import ipdb; ipdb.set_trace()
                col.save()
            return response


@plugin_pool.register_plugin
class GridColumnBootstrapPlugin(CMSPluginBase):
    model = GridColumnBootstrap
    name = _('Grid Column')
    module = _('Multi Columns')
    render_template = 'addons/column.html'
    allow_children = True
    form = GridColumnPluginForm

    def render(self, context, instance, placeholder):
        context.update({
            'column': instance,
            'placeholder': placeholder,
        })
        return context
