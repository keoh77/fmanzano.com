from functools import partial

from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import ugettext_lazy as _

BOOTSTRAP_GRID_CHOICES = [
    (i, f"{i} {_('columns')}")
    for i in range(1, 13)
]

ColumnSizeField = partial(
    models.IntegerField,
    default=0,
    choices=BOOTSTRAP_GRID_CHOICES,
    null=True,
    blank=True
)

class CustomClassesMixin:
    custom_classes = models.CharField(
        _('custom classes'),
        max_length=200,
        blank=True
    )


class GridBootstrap(CustomClassesMixin, CMSPlugin):

    def __str__(self):
        return _(f'{self.cmsplugin_set.all().count()} columns')


class GridColumnBootstrap(CustomClassesMixin, CMSPlugin):
    size_xs = ColumnSizeField(verbose_name=_('size (xs)'))
    size_sm = ColumnSizeField(verbose_name=_('size (sm)'))
    size_md = ColumnSizeField(verbose_name=_('size (md)'))
    size_lg = ColumnSizeField(verbose_name=_('size (lg)'))

    def __str__(self):
        result = []
        if self.size_xs:
            result.append(f'col-xs-{self.size_xs}')
        if self.size_sm:
            result.append(f'col-sm-{self.size_sm}')
        if self.size_md:
            result.append(f'col-md-{self.size_md}')
        if self.size_lg:
            result.append(f'col-lg-{self.size_lg}')
        return ' '.join(result)
