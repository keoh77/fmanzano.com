from .base import *
import os

DEBUG = False

ALLOWED_HOSTS = [
    "www.fmanzano.com",
    "fmanzano.com"
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'fmanzano',
        'USER': 'fmanzano_user',
        'PASSWORD': os.getenv("DB_PASS"),
        'HOST': os.getenv("DB_HOST"),
        'PORT': '5433',
    }
}